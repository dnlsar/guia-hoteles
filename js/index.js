$(function () {
  $('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle="popover"]').popover();
  $('.carousel').carousel({
    interval:3000
  });
  $('#contacto').on('shown.bs.modal', function () {
    console.log("Activamos el modal de contacto...");
    $("#contactoBtn").removeClass("btn-outline-success");
    $("#contactoBtn").addClass("btn-primary");
    $("#contactoBtn").prop("disabled",true);
  });
  $('#contacto').on('hide.bs.modal', function () {
    console.log("Cerrando el modal de contacto...");
    $("#contactoBtn").removeClass("btn-primary");
    $("#contactoBtn").addClass("btn-outline-success");
    $("#contactoBtn").prop("disabled",false);
  });
  $('#contacto').on('hidden.bs.modal', function () {
    console.log("Cerrado el modal de contacto...")
  });
});
